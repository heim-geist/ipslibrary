<?
	/**@ingroup entertainment
	 * @{
	 *
	 * @file          Entertainment_AllRoomesOff.ips.php
	 * @author        Andreas Brauneis
	 * @version
	 * Version 2.50.1, 31.01.2012<br/>
	 *
	 * Ausschalten aller Räume und Geräte
	 *
	 */
	include_once "Entertainment.inc.php";

	Entertainment_TurnOffAllRoomesAndDevices();

  /** @}*/
?>
